package chno;
import java.util.Scanner;

public class CHNO {
    static double C = 0.0, H = 0.0, N = 0.0, O = 0.0;
    static double roundMin = .35, roundMax = .65;
    static double smallest = 0, smallest1 = 0, smallest2 = 0;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(true){
        System.out.print("Enter Carbon Comp: ");
        C = input.nextDouble();
        System.out.print("Enter Hydrogen Comp: ");
        H = input.nextDouble();
        System.out.print("Enter Nitrogen Comp: ");
        N= input.nextDouble();
        System.out.print("Enter Oxygen Comp: ");
        O = input.nextDouble();
        if(C!=0)C/=12.0111;
        if(H!=0)H/=1.00794;
        if(N!=0)N/=14.0067;
        if(O!=0)O/=15.9994;
        if(C != 0 && H != 0 && N != 0 && O != 0){
        smallest1=Math.min(C,H);
        smallest2=Math.min(N,O);
        }
        if(C==0){
        smallest1=Math.min(H,N);
        smallest2=Math.min(smallest1, O);
        }
        if(H==0){
        smallest1=Math.min(C,N);
        smallest2=Math.min(smallest1, O);
        }
        if(N==0){
        smallest1=Math.min(H,C);
        smallest2=Math.min(smallest1, O);
        }
        if(O==0){
        smallest1=Math.min(H,N);
        smallest2=Math.min(smallest1, C);
        }
        smallest = Math.min(smallest1, smallest2);
        if(C!=0)C/=smallest;
        if(H!=0)H/=smallest;
        if(N!=0)N/=smallest;
        if(O!=0)O/=smallest;
        System.out.println("\nImperical Form");
        System.out.println("|C| Form: " + C + " |H| Form: " + H + " |N| Form: " + N + " |O| Form: " + O);
        if(C - (int) C > roundMin && C - (int) C < roundMax) C = (int) C + .5;
        if(C - (int) C > roundMax) C = (int) C + 1;
        if(H - (int) H > roundMin && H - (int) H < roundMax) H = (int) H + .5;
        if(H - (int) H < roundMin) H = (int) H;
        if(H - (int) H > roundMax) H = (int) H + 1;
        if(N - (int) N > roundMin && N - (int) N < roundMax) N = (int) N + .5;
        if(N - (int) N < roundMin) N = (int) N;
        if(N - (int) N > roundMax) N = (int) N + 1;
        if(O - (int) O > roundMin && O - (int) O < roundMax) O = (int) O + .5;
        if(O - (int) O < roundMin) O = (int) O;
        if(O - (int) O > roundMax) O = (int) O + 1;
        System.out.println("\nComuters Guess at Imperical Form (Rounding)");
        System.out.println("|C| Form: " + C + " |H| Form: " + H + " |N| Form: " + N + " |O| Form: " + O);
        if(C - (int) C == .5 || H - (int) H == .5 || N - (int) N == .5 || O - (int) O == .5){
            C*=2;H*=2;N*=2;O*=2;
        }
        System.out.println("\nComputer Guess at whole Imperical Form (Rounding then  Calculating)");
        System.out.println("|C| Form: " + C + " |H| Form: " + H + " |N| Form: " + N + " |O| Form: " + O);
        input.close();
        }
    }    
}
